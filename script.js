
/*
 * Extraction/scraping des résultats de recherche sur LinkedIn (personnes, entreprises)
 */

Scrap = {
    version         : '05/02/2019',
    placeId         : '#results_count',
    containerId     : '#li-search-extractor',
    getParams       : {},
    q               : "",
    type            : undefined,
    TYPE : {
        COMPANIES   : 'companies',
        PEOPLE      : 'people'
    },
    numResults      : 0,
    resultsPerPage  : 10,
    maxPage         : 100,
    results         : [],
    statusPages     : {},
    queries : {
        countExpected    : 0,
        countLoaded      : 0
    },
    sync:           false, // sync Ajax requests (one after another)
    timer           : 0,
    DOM             : {},
    fn              : {}
};

/**
 * Récupération des résultats de la recherche
 */
Scrap.fn.extractResults = function() {

    var _resultsPerPage = Scrap.container.find('select[name="results_per_page"] :selected').val();
    if( _resultsPerPage > 0 ) {
        Scrap.resultsPerPage = _resultsPerPage;
    }
    
    var _wantedResults  = Scrap.container.find('select[name="wanted_results"] :selected').val(),
        _lastPage       = Math.min( Scrap.maxPage, Math.ceil( _wantedResults / Scrap.resultsPerPage, 10 ) );

    Scrap.results = [];
    
    Scrap.fn.getSearchResults( 1, _lastPage );

    // Attente de la fin des requêtes
    Scrap.timer = setInterval( function() {

        Scrap.DOM.status.text( 'Chargement... (' + Scrap.queries.countLoaded + ' / ' + Scrap.queries.countExpected + ' pages de résultats)');

        if( Scrap.queries.countLoaded === Scrap.queries.countExpected ) {
            clearInterval( Scrap.timer );
            Scrap.DOM.status.hide();
            Scrap.fn.convertToCSV();
        }

    }, 500 );
};

/**
 * Récupération des résultats de recherche
 * @param {integer} firstPage
 * @param {integer} lastPage
 */
Scrap.fn.getSearchResults = function( firstPage, lastPage ) {
    
    if( !firstPage || !lastPage ) {
        firstPage   = 1;
        lastPage    = Math.min( Scrap.maxPage, Math.ceil( Scrap.numResults / Scrap.resultsPerPage, 10 ) );
    }
    Scrap.queries.countExpected = lastPage - firstPage + 1;
    Scrap.queries.countLoaded   = 0;
    
    if( Scrap.sync ) {
        var _page = firstPage,
            $xhr,
            _loadPage = function() {
                if( _page <= lastPage ) {
                    $xhr = Scrap.fn.loadResultsPage( _page );
                    $xhr.always( function() {
                        _page++;
                        _loadPage();
                    } );
                }
            };
        
        _loadPage();
        
    } else {
        for( var _page = firstPage; _page <= lastPage; _page++ ) {
            var $xhr = Scrap.fn.loadResultsPage( _page );
        }
    }
};

Scrap.fn.detectRequestCodeInPage = function() {
    $('code').each( function() {
        var _jsonStr = $(this).text().trim();
        //console.log( 'code tag', _jsonStr );
        if( _jsonStr.search('salesApiPeopleSearch') > 0 ) {
            var _json = JSON.parse( _jsonStr );
            console.log( _json );
            Scrap.apiRequestUrl = _json.request;
        }
    });
};

/**
 * Chargement d'une page de résultats
 * @param {integer} _page
 * @returns {jqXHR}
 */
Scrap.fn.loadResultsPage = function( _page ) {
    
    console.log( 'Result page #' + _page + ': loading...' );
    Scrap.statusPages[ 'page' + _page ] = 'loading';
    
    var _params = {};
    
    if( Scrap.isSalesNavigator ) {
        /*
         * https://www.linkedin.com/sales/search/results?keywords=...
         * > Page 1: start=0 & trackingInfoJson.requestId=fe220cae-5f5a-41ca-8c77-42da1bea2335
         * > Page 2; start=25 & trackingInfoJson.requestId=4c267019-ef36-4c69-b0b2-1d6f28f0c208
         */
        var _url = location.href.replace( '/sales/search', '/sales/search/results' );
        if( Scrap.apiRequestUrl ) {
            _url = Scrap.apiRequestUrl;
        }
        
        //var _baseUrl = 'https://www.linkedin.com/sales/search/people';
        
        // Cleaning changing parameters
        // Note: we don't parse the GET query because we lose multiple values that are defined on same facets
        _url = _url.replace( /&start=\d+/g, '' );
        _url = _url.replace( /&trackingInfoJson.requestId=[\w-]+/g, '' );
        
        if( !Scrap.apiRequestUrl ) {
            _params['start']                        = Scrap.resultsPerPage * ( _page - 1 );
            _params['trackingInfoJson.requestId']   = UUID.generate();
        }
        
        
    } else {
        //var _url = "https://www.linkedin.com/vsearch/cj?keywords=" + Scrap.q + "&page_num=" + _page;
        var _url = location.href.replace( location.pathname, location.pathname + 'j' ) + "&page_num=" + _page;
    }
    
    return $.getJSON( _url, _params, function( json ) {

        console.log( 'Result page #' + _page + ': loaded!' );
        console.log( json );
        Scrap.statusPages[ 'page' + _page ] = 'loaded';
        Scrap.queries.countLoaded++;

        if( Scrap.isSalesNavigator ) {
            var _results = json['searchResults'];
            
        } else {
            var _results = json['content']['page'].voltron_unified_search_json.search.results;
        }

        if( !_results ) {
            Scrap.statusPages[ 'page' + _page ] = 'nodata';
            console.log('No data');
            return;
        }

        for( var i in _results ) {
            var _fullResult = _results[i],
                _result;
            
            if( Scrap.isSalesNavigator ) {
                switch( Scrap.type ) {
                    case Scrap.TYPE.PEOPLE:
                        _result = {
                            member__memberId:           _fullResult.member.memberId,
                            member__formattedName:      _fullResult.member.formattedName,
                            member__title:              _fullResult.member.title,
                            member__location:           _fullResult.member.location,

                            company__companyId:         _fullResult.company.companyId,
                            company__companyName:       _fullResult.company.companyName
                        };
                        break;
                    case Scrap.TYPE.COMPANIES:
                        _result = _fullResult;
                        break;
                }
            } else {
                _result = _fullResult.person || _fullResult.company;
            }
            
            if( _result ) {
                Scrap.results.push( _result );
            }
        }

    });
};



/**
 * Extraction des sites web des entreprises
 * @param {boolean} use_input_urls
 */
Scrap.fn.extractCompanyPages = function( use_input_urls ) {
    
    console.info( 'Scrap.fn.extractCompanyPages...' );
    
    if( use_input_urls ) {
        var _urlsStr    = Scrap.container.find('textarea.urls-data').val().trim(),
            _urls       = _urlsStr.split('\n');
    
        if( !_urlsStr.length ) {
            alert('Please enter some company URLs in the input above.');
            return;
        }
        console.log( 'URLS:' );
        console.log( _urls );
        
        var _baseUrl = 'https://www.linkedin.com/company/';
        
        Scrap.results = [];
        for( var i = 0; i < _urls.length; i++ ) {
            var _url = _urls[i];
            if( _url ) {
                Scrap.results.push({ 'id' : _url.replace( _baseUrl, '' ) });
            }
        }
    }
    
    if( !Scrap.results.length ) {
        console.warn('Scrap.results is empty!');
        return;
    }
    
    Scrap.companyPages = {};
    
    Scrap.queries.countLoaded   = 0;
    Scrap.queries.countExpected = Scrap.results.length;
    
    
    // Attente de la fin des requêtes
    Scrap.timer = setInterval( function() {

        Scrap.DOM.status.text( 'Scraping company pages... (' + Scrap.queries.countLoaded + ' / ' + Scrap.queries.countExpected + ')').show();

        if( Scrap.queries.countLoaded === Scrap.queries.countExpected ) {
            clearInterval( Scrap.timer );
            Scrap.fn.convertToCSV();
        }

    }, 500 );
    
    // Requêtes
    for( var i in Scrap.results ) {

        var _result = Scrap.results[i];
    
        if( !_result || !_result['id'] ) {
            Scrap.queries.countExpected++;
            continue;
        }
        
        Scrap.fn.loadCompanyPage( _result['id'] );   
    }
    
};


/**
 * Extraction des sites web des entreprises à partir d'une liste d'URLs
 */
Scrap.fn.extractCompanyPagesFromURLs = function() {
    Scrap.fn.extractCompanyPages( true );
};

/**
 * Chargement d'une page de résultats
 * @param {integer} company_id
 */
Scrap.fn.loadCompanyPage = function( company_id ) {

    var _company_url = 'https://www.linkedin.com/company/' + company_id;
    
    console.log( 'Loading ' + _company_url );

    $.get( _company_url ).done( function( html ) {
        var _dataPage = Scrap.fn.extractDataFromCompanyPage( html );
        if( _dataPage !== false ) {
            Scrap.companyPages[ 'company:' + company_id ] = _dataPage;
        }
        
    }).fail( function() {
        console.warn( 'ERROR extracting data from company #' + company_id );
    }).always( function() {
        Scrap.queries.countLoaded++;
    });

};

/**
 * Get data from Linkedin Company page's HTML
 * @param {String} html
 * @returns {bool}
 */
Scrap.fn.extractDataFromCompanyPage = function( html ) {
    
    var $html = $(html),
        $code = $html.find('code#stream-promo-top-bar-embed-id-content');
    
    if( !$code.length ) {
        console.warn( 'No code found on the company page.', $html );
        return false;
    }

    var _json = JSON.parse( $code.html().replace('<!--','').replace('-->','') );

    if( !_json ) {
        return false;
    }
    
    var _data = {
            'name'              : _json.companyName || '',
            'website'           : _json.website || '',
            'industry'          : _json.industry || '',
            'type'              : _json.companyType || '',
            'country'           : typeof _json.headquarters !== 'undefined' ? _json.headquarters.country || '' : '', 
            'company_size'      : typeof _json.size !== 'undefined' ? _json.size.replace('+', 'plus') : '',
            'founded'           : _json.yearFounded || '',
            'followers_count'   : _json.followerCount || ''
        };
    
    return _data;
};


/**
 * Export des résultats en CSV
 */
Scrap.fn.convertToCSV = function() {

    Scrap.DOM.status.text('Converting to CSV...').show();

    Scrap.csv = "";
    
    Scrap.indexById = {};
    
    // Configuration
    var _config = '';
    
    switch( Scrap.type ) {
        
        case Scrap.TYPE.COMPANIES:
            _config = {
                'fields' : [
                    'companyId',
                    'name',
                    'industry',
                    'location',
                    'size'
                ],
                'columns' : [
                    'Company ID',
                    'Name',
                    'Industry',
                    'Location',
                    'Size',
                    'URL' // added in script
                ]
            };
            break;
            
        case Scrap.TYPE.PEOPLE:
            if( Scrap.isSalesNavigator ) {
                _config = {
                    'fields' : [
                        'member__memberId',
                        'member__formattedName',
                        'member__title',
                        'member__location',

                        'company__companyId',
                        'company__companyName'
                    ],
                    'columns' : [
                        'User ID', 'Full name', 'Job title', 'Location', 'Company ID', 'Company Name' /*'Structure (détectée)', 'Job (détecté)'*/
                    ]
                };
                
            } else {
                _config = {
                    'fields' : [
                        'id',
                        'firstName',
                        'lastName',
                        'fmt_headline',
                        'fmt_location',
                        'displayLocale',
                        'link_nprofile_view_3',
                        'fmt_industry'
                    ],
                    'columns' : [
                        'ID', 'Prenom', 'Nom', 'Intitule', 'Localisation', 'Langue', 'Profil Linkedin', 'Industrie', 'Structure (détectée)', 'Job (détecté)'
                    ]
                };
            }
            break;
    }

    if( !_config ) {
        console.warn('No config available in convertToCSV.');
        return;
    }
    
    // Colonnes
    switch( Scrap.type ) {
        case Scrap.TYPE.COMPANIES:
            if( Scrap.companyPages ) {

                _config.columns.push( 'Profil Linkedin' );

                var _companyFields = [
                    'name',
                    'website',
                    'industry',
                    'type',
                    //'address',
                    'country',
                    'company_size',
                    'founded',
                    'followers_count'
                ];
                for( var k in _companyFields ) {
                    _config.columns.push( _companyFields[k] );
                }
            }
            break;
    }
    
    Scrap.csv = _config.columns.join("\t") + "\n";
    
    
    // Données CSV
    for( var i in Scrap.results ) {

        var _result = Scrap.results[i],
            _id     = _result.id || _result.member__memberId || _result.companyId,
            _line   = [];
    
        if( !_result ) {
            console.log( 'No result on index ' + i );
            continue;
        }

        if( Scrap.indexById[ _id ] ) {
            Scrap.indexById[ _id ] += 1;
            continue;
        } else {
            Scrap.indexById[ _id ] = 1;
        }

        for( var j in _config.fields ) {
            _line.push( _result[ _config.fields[j] ] || '' );
        }
        
        switch( Scrap.type ) {
            
            case Scrap.TYPE.PEOPLE:
                
                if( Scrap.isSalesNavigator ) {
                    break;
                }
                
                var _snippets = _result.snippets || [],
                    _jobForStructure = '',
                    _structure = '',
                    _job = '';
                
                var _local_strings = {
                        'en' : {
                            separator: 'at',
                            currentCompany: 'Current'
                        },
                        'fr' : {
                            separator: 'chez',
                            currentCompany: "Entreprise actuelle "
                        }
                    },
                    _strings = _local_strings[ Scrap.lang ];
                    
                if( _strings !== undefined ) {
                    
                    for( var k = 0; k < _snippets.length; k++ ) {
                        var _snippet = _snippets[k];
                        if( _snippet['fieldName'] === _strings.currentCompany ) {
                            if( _snippet['fmt_heading'] ) {
                                _jobForStructure = _snippet['fmt_heading'];
                                break;
                            }
                            if( _snippet['bodyList'] && _snippet['bodyList'].length > 0 ) {
                                _jobForStructure = _snippet['bodyList'][0];
                                break;
                            }
                        }
                    }

                    if( _jobForStructure ) {
                        var _parts = decodeHtml( _jobForStructure ).split( ' '+_strings.separator+' ' );
                        if( _parts.length > 1 ) {
                            _structure  = _parts.pop();
                            _job = _parts.join('');
                        }
                    }
                }
                
                _line.push( _structure );
                _line.push( _job );
                break;
                
            case Scrap.TYPE.COMPANIES:
                _line.push( 'https://www.linkedin.com/company/' + _id );

                if( Scrap.companyPages && Scrap.companyPages[ 'company:'+_id ] ) {
                    for( var k in _companyFields ) {
                        _line.push( Scrap.companyPages[ 'company:'+_id ][_companyFields[k]] || '' );
                    }
                }
                break;
        }
        
        Scrap.csv += _line.join("\t") + "\n";

    }
    
    Scrap.csv = strip_tags( decodeHtml( Scrap.csv ) );
    
    // UI
    Scrap.DOM.status.text('CSV is ready (' + Scrap.results.length + ' results)').show();
    var $textarea = Scrap.container.find('textarea.csv-data');
    $textarea.show();
    $textarea.val( Scrap.csv );
    $textarea.select();
};

Scrap.fn.testIP = function() {
    $.get( 'https://www.whatismyip.com/' ).always( function( html ) { console.log( html ); } );
};


/*****************************
 *      Helper functions     *
 *****************************/

Scrap_Utilities = {};

/**
 * Retourne les paramètres GET de l'URL
 * @return Object
 */
Scrap_Utilities.extractGetParams = function() {    
    return Scrap_Utilities.deserialize( window.location.search.substr(1) );
};

/**
 * Transforme une chaine de type param1=value1&param2=value2... 
 * sous forme d'un objet littéral { 'param1' : 'value1', 'param2' : 'value2' }
 * @param {String} str
 * @return {Object}
 */
Scrap_Utilities.deserialize = function( str ) {

    var _object  = {},
        _couples = str.split("&"),
        _keyValue;
    
    for( var i = 0; i < _couples.length; i++ ) {
        _keyValue = _couples[i].split("=");
        _object[ decodeURIComponent( _keyValue[0] ) ] = ( _keyValue.length > 1 ) ? decodeURIComponent( _keyValue[1] ) : "";
    }
    
    return _object;

};

// http://stackoverflow.com/a/7394787
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

// http://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
function strip_tags( txt ) {
    return txt.replace(/(<([^>]+)>)/ig,"");
}

/**
 * Fast UUID generator, RFC4122 version 4 compliant.
 * @author Jeff Ward (jcward.com).
 * @license MIT license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
var UUID = (function() {
  var self = {};
  var lut = []; for (var i=0; i<256; i++) { lut[i] = (i<16?'0':'')+(i).toString(16); }
  self.generate = function() {
    var d0 = Math.random()*0xffffffff|0;
    var d1 = Math.random()*0xffffffff|0;
    var d2 = Math.random()*0xffffffff|0;
    var d3 = Math.random()*0xffffffff|0;
    return lut[d0&0xff]+lut[d0>>8&0xff]+lut[d0>>16&0xff]+lut[d0>>24&0xff]+'-'+
      lut[d1&0xff]+lut[d1>>8&0xff]+'-'+lut[d1>>16&0x0f|0x40]+lut[d1>>24&0xff]+'-'+
      lut[d2&0x3f|0x80]+lut[d2>>8&0xff]+'-'+lut[d2>>16&0xff]+lut[d2>>24&0xff]+
      lut[d3&0xff]+lut[d3>>8&0xff]+lut[d3>>16&0xff]+lut[d3>>24&0xff];
  }
  return self;
})();


/*
 * Initialisation
 */
Scrap.fn.init = function() {
    
    if( location.host !== 'www.linkedin.com' ) {
        alert( 'Please go to LinkedIn first :)' );
        return;
    }
    
    /*
     * Vérification que la page est OK
     */
    switch( location.pathname.replace(/\/$/,'') ) {
        /* 
         * V1
         */
        case '/vsearch/c':
            Scrap.type = Scrap.TYPE.COMPANIES;
            break;
        case '/':
            Scrap.type = Scrap.TYPE.COMPANIES;
            Scrap.placeId = '.header';
            break;
        case '/vsearch/p':
            Scrap.type = Scrap.TYPE.PEOPLE;
            break;
        /* 
         * V2
         */
        case '/search/results/people':
            Scrap.type = Scrap.TYPE.PEOPLE;
            Scrap.placeId = '.search-results__total';
            Scrap.isV2 = true;
            break;
        case '/search/results/companies':
            Scrap.type = Scrap.TYPE.COMPANIES;
            Scrap.placeId = '.search-results__total';
            Scrap.isV2 = true;
            Scrap.resultsPerPage = 25;
            break;
        case '/sales/search':
        case '/sales/search/people':
            Scrap.type = Scrap.TYPE.PEOPLE;
            Scrap.placeId = '.search-results__action-bar';
            Scrap.isV2 = true;
            Scrap.isSalesNavigator = true;
            Scrap.resultsPerPage = 25;
            break;
        case '/sales/search/companies':
        //case '/sales/search/company':
            Scrap.type = Scrap.TYPE.COMPANIES;
            Scrap.placeId = '.search-results__action-bar';
            Scrap.isV2 = true;
            Scrap.isSalesNavigator = true;
            Scrap.resultsPerPage = 25;
            break;
        default:
            Scrap.type = undefined;
            break;
    }    
    
    if( !Scrap.type ) {
        alert( 'Please go to a LinkedIn Search (Companies or People) page first :)' );
        return;
    }
    
    // Language
    var _locale = typeof LI !== 'undefined' ? LI.i18n.getLocale() : { country: "FR", language: "fr", value: "fr_FR" };
    Scrap.lang = _locale.language || '';
    
    Scrap.fn.detectRequestCodeInPage();
    
    /*
     * UI
     */
    if( $( Scrap.containerId ).length > 0 ) {
        $( Scrap.containerId ).remove();
    }
    
    var $place = $( Scrap.placeId );
    if( $place.length === 0 ) {
        console.warn( 'Place not found. Scrap.placeId =', Scrap.placeId );
    }
    $place.after(
        '<div id="' + Scrap.containerId.replace('#','') + '" class="type-' + Scrap.type + '">' + 
            // Extract results (people or companies)
            '<p class="step step-extract-results">' + 
                '<select name="wanted_results"></select> ' + Scrap.type + ' &nbsp;&nbsp;&nbsp; \
                <select name="results_per_page">\
                    <option value="10">10</option>\
                    <option value="25" selected>25</option>\
                </select> results per page' +
                '<button class="extract-results scrap-btn-primary" type="button">Extract Results</button>' +
            '</p>' +
            // Extract company pages
//            '<p class="step step-extract-company-pages">' + 
//                'Extract company pages:' +
//                '<button class="convert-csv scrap-btn-secondary" type="button">Get results (CSV)</button>' +
//                '<button class="extract-company-pages scrap-btn-primary" type="button">Extract companies pages</button>' + 
//            '</p>' +
            // Extract company pages from URLs
//            '<div class="step step-extract-company-pages-urls">' + 
//                '<textarea class="urls-data" placeholder="List of LinkedIn Company URLs"></textarea/>' +
//                '<p><button class="extract-company-pages-urls scrap-btn-primary" type="button">Extract companies pages from URLs</button></p>' + 
//                '<br>' +
//            '</div>' +
            // CSV
            '<div class="step-csv-done">' + 
                '<textarea class="csv-data" placeholder="CSV data"></textarea/>' +
            '</div>' +
            // Status
            '<div class="status"></div>' +
            '<div class="scrap-version">Version: ' + Scrap.version + '</div>' +
        '</div>'
    );
    $('body').append(
        '<style>\
            #li-search-extractor { background: #eee; padding: 20px; position: relative; border: 1px solid #ccc; border-left: 0; border-right:0; } \
            \
            #li-search-extractor button { border: 0; border-radius: 2px; float: right; margin-left: 1em; font-size: 14px; padding: 3px 16px; } \
            #li-search-extractor .scrap-btn-primary { background: #008cc9; color: #fff; } \
            #li-search-extractor .scrap-btn-secondary { background: #fff; color: #008cc9; } \
            #li-search-extractor textarea { border:1px solid #ccc; font-family:Arial; font-size:12px; height:8em; margin:10px 0; resize:none; width:100% } \
            #li-search-extractor .csv-data { display:none; } \
            #li-search-extractor .step { margin-bottom: 10px; } \
            #li-search-extractor .step-extract-company-pages { border-top: 1px solid #aaa; padding-top: 15px; margin-top: 15px; } \
            #li-search-extractor:not(.type-companies) .step-extract-company-pages, \
            #li-search-extractor:not(.type-companies) .step-extract-company-pages-urls { \
                display:none; \
            }\
            #li-search-extractor select { width: auto; } \
            #li-search-extractor .status { color: green; } \
            #li-search-extractor .scrap-version { font-size: 10px; color: #777; font-weight: 300; box-sizing: border-box; position: absolute; left: 0; bottom: 0; text-align: right; padding: 0 7px; width: 100%; } \
        </style>'
    );

    /*
     * Initialisation
     */
    Scrap.container     = $( Scrap.containerId );
    Scrap.DOM.status    = Scrap.container.find('.status');
    Scrap.getParams     = Scrap_Utilities.extractGetParams();
    Scrap.q             = Scrap.getParams.keywords || '';
    Scrap.numResults    = 1000; //parseInt( $place.text().replace(/[^0-9]/g,"").replace(',',''), 10 );
    var _select = '',
        _minSelect = 2 * Scrap.resultsPerPage;
    _select += '<option value="' + _minSelect + '">' + _minSelect + '</option>';
    for( var i = 100; i <= 1000; i = i + 100 ) {
        if( i < Scrap.numResults ) {
            _select += '<option value="' + i + '">' + i + '</option>';
        } else {
            _select += '<option value="' + Scrap.numResults + '">' + Scrap.numResults + '</option>';
            break;
        }
    }
    Scrap.container.find('select[name="wanted_results"]').html( _select );
    Scrap.container.find('select[name="wanted_results"] :last-child').attr('selected','selected');

    /*
     * Boutons d'action
     */
    Scrap.container.find('.extract-results').click( Scrap.fn.extractResults );

    Scrap.container.find('.extract-company-pages').click( function() { Scrap.fn.extractCompanyPages(); } );
    Scrap.container.find('.extract-company-pages-urls').click( function() { Scrap.fn.extractCompanyPagesFromURLs(); } );

    Scrap.container.find('.convert-csv').click( Scrap.fn.convertToCSV );
    
    
    // Input contenant du texte à copier
    Scrap.container.find('textarea.csv-data').click( function() {
        $(this).select();
    });

};

/*
 * Make sure a modern version (>= 1.7) of jQuery is available before launching the script
 */
!function( $ ) {
    var loadBookmarklet = function( $ ) {
        Scrap.fn.init();
    };
    if( $ && $.fn && parseFloat( $.fn.jquery ) >= 1.7 ) {
        loadBookmarklet();
    } else {
        var s = document.createElement( "script" );
        s.src = "//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js";
        s.onload = s.onreadystatechange = function() {
            var state = this.readyState;
            state && "loaded" !== state && "complete" !== state || loadBookmarklet( jQuery.noConflict() );
        };
        document.getElementsByTagName( "head" )[0].appendChild( s );
    }
}( window.jQuery );